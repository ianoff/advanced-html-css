# GDI Advanced HTML/CSS

This is the first draft at a Girl Develop It Chicago Advanced HTML/CSS curriculum. It has thus far been developed by Meagan Evanoff.

The course is meant to be taught in 4 two- to two-and-a-half-hour sections. Each of the slides and practice files are customizable according to the needs of a given class or audience.

## Classes

### Class 1

Review of CSS Positioning from the Intro & Intermediate classes. 

- Absolute and relative positioning
- Floats and Clears
- The modern CSS grid system

### Class 2

Building our mockup
- Review of responsive design
- Review the mockup
- Live coding the build

### Class 3

Images, other assets, and CSS performance

### Class 4

- Cross-browser testing and debugging
- Introduction to template languages

```
You can change transition by changing the reveal transition property in Reveal.initialize
```javascript
  Reveal.initialize({
  				transition:  'default', // default/cube/page/concave/zoom/linear/none
  			});
```